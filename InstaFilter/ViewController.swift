//
//  ViewController.swift
//  InstaFilter
//
//  Created by Hariharan S on 02/06/24.
//

import CoreImage
import UIKit

class ViewController: UIViewController {
    
    // MARK: - IBOutlets

    @IBOutlet private weak var intensity: UISlider!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var changeFilterButton: UIButton!
    
    // MARK: - Properties

    var context: CIContext!
    var currentImage: UIImage!
    var currentFilter: CIFilter!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Filter"
        self.context = CIContext()
        self.currentFilter = CIFilter(name: "CISepiaTone")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(self.importPicture)
        )
    }
}

// MARK: - IBActions

private extension ViewController {
    @IBAction func save(_ sender: UIButton!) {
        guard let image = self.imageView.image
        else {
            self.showNoImageAlert()
            return
        }
        UIImageWriteToSavedPhotosAlbum(
            image,
            self,
            #selector(image(_:didFinishSavingWithError:contextInfo:)),
            nil
        )
    }
    
    @IBAction func changeFilter(_ sender: UIButton!) {
        let ac = UIAlertController(
            title: "Choose filter",
            message: nil,
            preferredStyle: .actionSheet
        )
        ac.addAction(
            UIAlertAction(title: "CIBumpDistortion", style: .default, handler: self.setFilter)
        )
        ac.addAction(
            UIAlertAction(title: "CIGaussianBlur", style: .default, handler: self.setFilter)
        )
        ac.addAction(
            UIAlertAction(title: "CIPixellate", style: .default, handler: self.setFilter)
        )
        ac.addAction(
            UIAlertAction(title: "CISepiaTone", style: .default, handler: self.setFilter)
        )
        ac.addAction(
            UIAlertAction(title: "CITwirlDistortion", style: .default, handler: self.setFilter)
        )
        ac.addAction(
            UIAlertAction(title: "CIUnsharpMask", style: .default, handler: self.setFilter)
        )
        ac.addAction(
            UIAlertAction(title: "CIVignette", style: .default, handler: self.setFilter)
        )
        ac.addAction(
            UIAlertAction(title: "Cancel", style: .cancel)
        )
        self.present(
            ac, 
            animated: true
        )
    }
    
    @IBAction func intensityChanged(_ sender: UISlider!) {
        if self.currentImage == nil {
            self.showNoImageAlert()
            return
        }
        self.applyProcessing()
    }
    
    @objc func importPicture() {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        self.present(picker, animated: true)
    }
}

// MARK: - Private Methods

private extension ViewController {
    func applyProcessing() {
        let inputKeys = self.currentFilter.inputKeys
        if inputKeys.contains(kCIInputIntensityKey) {
            self.currentFilter.setValue(
                intensity.value, 
                forKey: kCIInputIntensityKey
            )
        }
        if inputKeys.contains(kCIInputRadiusKey) {
            self.currentFilter.setValue(
                intensity.value * 200,
                forKey: kCIInputRadiusKey
            )
        }
        if inputKeys.contains(kCIInputScaleKey) {
            self.currentFilter.setValue(
                intensity.value * 10,
                forKey: kCIInputScaleKey
            )
        }
        if inputKeys.contains(kCIInputCenterKey) {
            self.currentFilter.setValue(
                CIVector(x: currentImage.size.width / 2, y: currentImage.size.height / 2),
                forKey: kCIInputCenterKey
            )
        }

        if let cgimg = context.createCGImage(
            self.currentFilter.outputImage!,
            from: currentFilter.outputImage!.extent
        ) {
            let processedImage = UIImage(cgImage: cgimg)
            self.imageView.image = processedImage
        }
    }
    
    func setFilter(action: UIAlertAction) {
        guard currentImage != nil 
        else {
            self.showNoImageAlert()
            return
        }
        guard let actionTitle = action.title 
        else {
            return
        }
        self.changeFilterButton.setTitle(
            actionTitle,
            for: .normal
        )
        self.currentFilter = CIFilter(name: actionTitle)
        let beginImage = CIImage(image: self.currentImage)
        self.currentFilter.setValue(
            beginImage,
            forKey: kCIInputImageKey
        )
        self.applyProcessing()
    }
    
    func showNoImageAlert() {
        let ac = UIAlertController(
            title: "Oops!, No Image",
            message: nil,
            preferredStyle: .alert
        )
        ac.addAction(
            UIAlertAction(title: "OK", style: .default)
        )
        self.present(ac, animated: true)
    }
    
    @objc func image(
        _ image: UIImage, 
        didFinishSavingWithError error: Error?,
        contextInfo: UnsafeRawPointer
    ) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(
                title: "Save error",
                message: error.localizedDescription, 
                preferredStyle: .alert
            )
            ac.addAction(
                UIAlertAction(title: "OK", style: .default)
            )
            self.present(ac, animated: true)
        } else {
            let ac = UIAlertController(
                title: "Saved!",
                message: "Your altered image has been saved to your photos.",
                preferredStyle: .alert
            )
            ac.addAction(
                UIAlertAction(title: "OK", style: .default)
            )
            self.present(ac, animated: true)
        }
    }
}

// MARK: - UIImagePickerControllerDelegate Conformance

extension ViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
    ) {
        guard let image = info[.editedImage] as? UIImage
        else {
            return
        }
        self.dismiss(animated: true)
        self.currentImage = image
        
        let beginImage = CIImage(image: self.currentImage)
        self.currentFilter.setValue(
            beginImage, 
            forKey: kCIInputImageKey
        )
        self.applyProcessing()
    }
}
